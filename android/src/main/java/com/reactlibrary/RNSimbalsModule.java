
package com.reactlibrary;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.simbals.livematch.SimbalsLiveMatch;
import com.simbals.livematch.SimbalsLiveMatchNotifier;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import android.content.Intent;

public class RNSimbalsModule extends ReactContextBaseJavaModule implements SimbalsLiveMatchNotifier {

  private static final String LOGTAG = "RNSimbalsModule";

  private final ReactApplicationContext reactContext;
  private SimbalsLiveMatch _liveMatch;
  private Map<String, String> _channels;
  private Callback _callbackSuccess;
  private Callback _callbackError;
  private String _token;
  private boolean _started = false;
  private boolean _shouldStop = false;

  private static final String RESPONSE_RECEIVED_SUCCESS = "rnSimbalsModuleResponseSuccessReceived";
  private static final String RESPONSE_RECEIVED_ERROR = "rnSimbalsModuleResponseErroReceived";
  private static final String RESPONSE_MESSAGE_RECEIVED = "rnSimbalsModuleResponseMessageReceived";

  public RNSimbalsModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    //super.getReactApplicationContext().addLifecycleEventListener(this);
  }

  @ReactMethod
  public void initializeWithToken(String token){
    _token = token;
    
    initLiveMatch();
    
    _channels = new HashMap<>();
    _channels.put("12", "Chérie FM");
    _channels.put("4", "Fun Radio");
    _channels.put("14", "M Radio");
    _channels.put("5", "Nostalgie");
    _channels.put("2", "NRJ");
    _channels.put("16", "RFM");
    _channels.put("7", "RTL2");
    _channels.put("8", "SkyRock");
    _channels.put("9", "Virgin Radio");
    _channels.put("201", "Radio FG");
    _channels.put("209", "Kiss FM");
    _channels.put("210", "Radio Émotion");
  }

  @ReactMethod
  public void start(Callback callback) {
    _shouldStop = false;
    
    System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& START");

    try {
      _liveMatch.start();
      _started = true;
      System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& START SUCCESS");
    } catch(Exception e) {
      System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& START ERROR");
      e.printStackTrace();
      _started = false;
    }

    try {
      callback.invoke(_started);
    }catch(Exception e) {
      System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& START CALLBACK ERROR");
      e.printStackTrace();
    }
    
  }
  
  @ReactMethod
  public void isRunning(Promise promise) {
    if(_started) promise.resolve(true);
    else promise.reject("server_not_started", "server not started");
  }

  @ReactMethod
  public void stop(Callback callback) {
    _shouldStop = true;
    System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& STOP");
    if(_liveMatch != null) {
      try {
        _liveMatch.stop();
        _started = false;
        System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& STOP SUCCESS");
      } catch(Exception e) {
        System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& STOP ERROR");
        e.printStackTrace();
        _started = true;
      }
    }

    try {
      callback.invoke(!_started);
    } catch(Exception e) {
      System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& STOP CALLBACK ERROR");
      e.printStackTrace();
    }
    
  }

  @ReactMethod
	public void setNewCallBack(Callback successCallback, Callback errorCallback) {	
    if(_callbackSuccess == null) _callbackSuccess = successCallback;
    if(_callbackError == null) _callbackError = errorCallback;
	}	

  @Override
  public String getName() {
    return "RNSimbalsModule";
  }

  @Override
  public void simbalsLiveMatchDidFinish(String channel, int message, boolean success) {
    
    if (_liveMatch != null && _callbackSuccess != null && _callbackError != null) {
	  	if (success && channel != null) {  	
        _callbackSuccess.invoke(channel);
	    } else {
        _callbackError.invoke("error - no radio");
    	}
    } else if(_callbackError != null) {
      _callbackError.invoke("error - no radio");
    }

    _callbackSuccess = null;
    _callbackError = null;
    _started = false;
    System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& SUCCESS CHANNEL " + channel);
	}

	@Override
  public void simbalsLiveMatchMessages(int message) {
    if(message == 34) _started = _shouldStop == false;
    System.out.println("&&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& &&&&&&&&&&&&&&&&&&&&& RECEIVE MESSAGE {" + Integer.toString(message) + "}");
  }

  public void initLiveMatch() {
    _liveMatch = new SimbalsLiveMatch(getCurrentActivity().getCacheDir().getAbsolutePath(), _token , getCurrentActivity().getExternalFilesDir(null) + File.separator + "SimbalsLivematch");  
    _liveMatch.registerNotifier(this);
  }
}