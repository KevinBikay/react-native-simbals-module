//
//  SimbalsLiveMatch.h
//  SimbalsLiveMatch
//
//  Created by florian on 22/07/2015.
//  Copyright (c) 2015 Simbals. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SimbalsLiveMatch;

@protocol SimbalsLiveMatchDelegate;

@interface SimbalsLiveMatch : NSObject {
    
@private
    id <SimbalsLiveMatchDelegate> _delegate;
}

@property (retain,nonatomic) id delegate;

/*!
 constructor of the SimbalsLiveMatch class.
 Perform the initialization of the object and the initial setup.
 @param op
 The output path of the recorded samples used for the identification. Can be [NSHomeDirectory() stringByAppendingPathComponent:\@"Documents"]
 @param clientToken
 Token to access simbals WebAPI
 */
- (id) initWithOutPath:(NSString*)op token:(NSString*)token;

/*!
 Use this method to start the identification process
 */
- (void) start;

/*!
 Use this method to stop the identification process
 */
- (void) stop;

@end

/*!
 @protocol
 SimbalsLiveMatchDelegate
 @discussion
 This protocol has to be implemented by the front app to interact with the identification process
 */
@protocol SimbalsLiveMatchDelegate <NSObject>

@required
/*!
 This method is required and must be implemented in the delegate
 It is called each time identification is finished
 @param channelId
 provides the of the identified track. Nil if uncessful
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the identification was successful
 */
- (void) simbalsLiveMatchDidFinish:(NSString*)channelId message:(int)message successfully:(BOOL)success;

@optional
/*!
 This method is optional and provides debug informations during the identification
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 */
- (void) simbalsLiveMatchMessages:(int)message;

@end
