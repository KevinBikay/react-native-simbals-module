
#import "RNSimbalsModule.h"
#define isNSNull(value) [value isKindOfClass:[NSNull class]]

@implementation RNSimbalsModule
{
  bool hasListeners;
}

RCT_EXPORT_MODULE();

RCTResponseSenderBlock callbackAction;
RCTResponseSenderBlock callbackActionFail;
BOOL isCallbackCallable;

NSString * _token;

-(void)startObserving {
    hasListeners = YES;
    // Set up any upstream listeners or background tasks as necessary
}

-(void)stopObserving {
    hasListeners = NO;
    // Remove upstream listeners, stop unnecessary background tasks
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"rnSimbalsModuleResponseSuccessReceived", @"rnSimbalsModuleResponseErroReceived", @"rnSimbalsModuleResponseMessageReceived"];
}

RCT_EXPORT_METHOD(initializeWithToken:(NSString *) token) {

	_token = token;

	_channelsList = @{
					  @"12": @"Chérie FM",
					  @"4": @"Fun Radio",
					  @"14": @"M Radio",
					  @"5": @"Nostalgie",
					  @"2": @"NRJ",
					  @"16": @"RFM",
					  @"7": @"RTL2",
					  @"8": @"SkyRock",
					  @"9": @"Virgin Radio",
					  @"201": @"Radio FG",
					  @"209": @"Kiss FM",
					  @"210": @"Radio Émotion"
					  };

	_started = false;
	_shouldStop = false;
}

RCT_EXPORT_METHOD(setNewCallBack:(RCTResponseSenderBlock)callback : (RCTResponseSenderBlock)callbackFail) {
	callbackAction = callback;
	callbackActionFail = callbackFail;
    isCallbackCallable = TRUE;
}

RCT_EXPORT_METHOD(start: (RCTResponseSenderBlock)callback) {
	//upon click on the identify button (UI), we dispatch the connect method of SimbalsConnect
	// SimbalsConnect is responsible for thread management
	// it is in charge of audio recording and processing
    
	[self initSimbals];
    
	NSLog(@"---> START ***************************************************");
	
	@try {
		[_liveMatch start];
		NSLog(@"---> STARTED ***************************************************");
		_started = true;
	}
	@catch (NSException *exception) {
		NSLog(@"---> START FAILED --> %@", exception.reason);

		_started = false;
	}
	
	_shouldStop = false;

	callback(@[[NSNull null]]);
}

RCT_EXPORT_METHOD(stop: (RCTResponseSenderBlock)callback) {
	//upon click on the identify button (UI), we dispatch the connect method of SimbalsConnect
	// SimbalsConnect is responsible for thread management
	// it is in charge of audio recording and processing

	NSLog(@"---> STOP ***************************************************");

	@try {
		if(_started) {
			[_liveMatch stop];
			NSLog(@"---> STOPPED ***************************************************");
		} 
		_started = false;
	}
	@catch (NSException *exception) {
		_started = true;
		NSLog(@"---> STOP FAILED --> %@", exception.reason);
	}
	
	_shouldStop = true;

	callback(@[[NSNull null]]);
}

RCT_REMAP_METHOD(isRunning, findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    
    NSError *error = nil;
    
    if(_started) resolve(@[[NSNull null]]);
    else reject(@"not_started", @"The server is not started", error);
}

// Called when identification is done, either successful or not
- (void) simbalsLiveMatchDidFinish:(NSString*)channelId message:(int)message successfully:(BOOL)success {
	//This message is dispatched only when SimbalsIdentify has finished, either with success or failure
	//So, real apps should check success to handle failure properly
	if(!success){
		NSString *completeStatus = @"error - no radio";

		NSLog(@"---> ERROR RADIO NOT FOUND ***************** %@", completeStatus);

		//if (hasListeners) {
		//	[self sendEventWithName:@"rnSimbalsModuleResponseErroReceived" body:@{@"channel": completeStatus}];
		//}
		
		if(isCallbackCallable == TRUE) callbackActionFail(@[[NSNull null], completeStatus]);

		//[_liveMatch start];

	}else {
		NSLog(@"---> SUCCESS RADIO FOUND ***************** %@", channelId);

		//Id has been successful and returns the id of the identified radio in the _channelsList
		// you can use this id to display any info related to identified radio
		// NSString *completeStatus = [self updateConsole: [NSString stringWithFormat:@"%@ - %@", message, channelId]];
		//if (hasListeners) {
		//	[self sendEventWithName:@"rnSimbalsModuleResponseSuccessReceived" body:@{@"channel": channelId}];
		//}
		if(isCallbackCallable == TRUE) callbackAction(@[[NSNull null], channelId]);
		// if(_shouldStop == false){
		// 	// sleep for 10s and then check if we are still on the same radio
		// 	[NSThread sleepForTimeInterval:10];

		//@try {
		//	[_liveMatch start];
		//}
		//@catch (NSException *exception) {
		//	NSLog(@"%@", exception.reason);
		//}
		
		// }
	}
    _started = false;
    isCallbackCallable = FALSE;
}

- (void) simbalsLiveMatchMessages:(int)message {
	
	if(message == 34) {
		_started = _shouldStop == false;
	}
	
	@try {
		NSString *messageStr= [SimbalsMessages getTranslationFR: message]; //[NSString stringWithFormat:@"%d",message];

		NSLog(@" --->RECEIVE MESSAGE *************************************************** --> %@", messageStr);
	}
	@catch (NSException *exception) {
		NSLog(@"---> ERROR GET MESSAGE --> %@", exception.reason);
	}

	/*if (hasListeners) {
		NSString *messageStr= [NSString stringWithFormat:@"%d",message];
		[self sendEventWithName:@"rnSimbalsModuleResponseMessageReceived" body:@{@"message": messageStr}];
	}*/
}

- (void) initSimbals {
	NSLog(@"---> INIT SIMBALS *************************************************** %@", _token);
	_liveMatch = [[SimbalsLiveMatch alloc] initWithOutPath:[NSString stringWithFormat:@"%@", [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]] token: _token];
	[_liveMatch setDelegate:self];
}

- (NSString*) updateConsole:(NSString*)message {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	return [NSString stringWithFormat:@"%@ : %@", [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:0]], message];
}

@end
