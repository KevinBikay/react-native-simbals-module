//
//  SimbalsMessages.h
//  SimbalWebAPIClient
//
//  Created by florian on 17/03/2014.
//  Copyright (c) 2014 Simbals. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
 @definedblock Simbals messages
 @define SIMBALS_SYNC_READY Synchronisation process is ready
 @define SIMBALS_SYNC_PAUSE There's no active synchronisation process
 @define SIMBALS_SYNC_SYNCRONIZED The synchronization process has been successful
 @define SIMBALS_SYNC_NOT_SYNCHRONIZED The synchronization has been lost
 @define SIMBALS_SYNC_SYNCHRONIZING Try to synchronize the audio sample to the main stream
 @define SIMBALS_SYNC_SYNCHRONIZATION_ERROR The synchronization process has failed
 @define SIMBALS_SYNC_STOPPED The synchronization process is stopped
 @define SIMBALS_SYNC_ERROR Unknown error
 @define SIMBALS_CONNECT_READY We are ready to process
 @define SIMBALS_CONNECT_IDENTIFIED The identification has been successful
 @define SIMBALS_CONNECT_NOT_IDENTIFIED The identification did not succeed
 @define SIMBALS_CONNECT_IDENTFICATION_ERROR An error occurred during the identification
 @define SIMBALS_CONNECT_IDENTIFYING Try to identify the audio sample to the main stream
 @define SIMBALS_CONNECT_STOPPED The identification process is stopped
 @define SIMBALS_CONNECT_ERROR Unknown error
 @define SIMBALS_MUSICID_READY MusicId SDK is ready
 @define SIMBALS_MUSICID_IDENTIFIED The musicid process has been successful
 @define SIMBALS_MUSICID_NOT_IDENTIFIED The musicid process did not succeed
 @define SIMBALS_MUSICID_IDENTIFYING Try to identify the audio stream
 @define SIMBALS_MUSICID_IDENTIFICATION_ERROR The identificaiton has failed
 @define SIMBALS_MUSICID_STOPPED The identification process is stopped
 @define SIMBALS_MUSICID_ERROR Unknown error
 @define SIMBALS_SWAPI_READY WebAPI client is ready to process requests
 @define SIMBALS_SWAPI_OPENING_SESSION The Client try to open a session on the SWAPI
 @define SIMBALS_SWAPI_SESSION_OPENED The session is opened on the SWAPI
 @define SIMBALS_SWAPI_NO_SESSION The session is not opened on the SWAPI
 @define SIMBALS_SWAPI_JOB_NOT_STARTED Unable to start the job on SWAPI
 @define SIMBALS_SWAPI_JOB_STARTED The job on SWAPI is started
 @define SIMBALS_SWAPI_JOB_FINISHED The job on SWAPI is finished
 @define SIMBALS_SWAPI_JOB_FAILED The job on SWAPI has failed
 @define SIMBALS_SWAPI_JOB_NOT_READY The job on SWAPI is not finished yet
 @define SIMBALS_SWAPI_MUSICID_CALL_FAILED The call to musicid API failed
 @define SIMBALS_SWAPI_LIVEMATCH_CALL_FAILED The call to livematch API failed
 @define SIMBALS_SWAPI_METADATA_NOT_FOUND No corresponding metadata were found
 @define SIMBALS_SWAPI_METADATA_FOUND Metadata were found
 @define SIMBALS_GENERAL_RECORDING The SDK is recording audio
 @define SIMBALS_GENERAL_RECORDING_ERROR An error occurred during the recording of the audio sample
 @define SIMBALS_GENERAL_RECORDING_SUCCESS The recording of the audio sample has been successfull
 @define SIMBALS_GENERAL_AUDIOSESSION_ERROR An error has occured during audio session initialization
 @define SIMBALS_GENERAL_INPUT_LEVEL_TOO_LOW The audio input level is too low
 @define SIMBALS_GENERAL_INPUT_LEVEL_LOW The audio input level is low
 @define SIMBALS_GENERAL_INPUT_LEVEL_MEDIUM The audio input level is good enough
 @define SIMBALS_GENERAL_INPUT_LEVEL_GOOD The audio input level is good
 @define SIMBALS_LIVEMATCH_READY MusicId SDK is ready
 @define SIMBALS_LIVEMATCH_IDENTIFIED The musicid process has been successful
 @define SIMBALS_LIVEMATCH_NOT_IDENTIFIED The musicid process did not succeed
 @define SIMBALS_LIVEMATCH_IDENTIFYING Try to identify the audio stream
 @define SIMBALS_LIVEMATCH_IDENTIFICATION_ERROR The identificaiton has failed
 @define SIMBALS_LIVEMATCH_STOPPED The identification process is stopped
 @define SIMBALS_LIVEMATCH_ERROR Unknown error

 */

//messages
#define SIMBALS_SYNC_READY 0
#define SIMBALS_SYNC_PAUSE 1
#define SIMBALS_SYNC_SYNCRONIZED 2
#define SIMBALS_SYNC_NOT_SYNCHRONIZED 3
#define SIMBALS_SYNC_SYNCHRONIZING 4
#define SIMBALS_SYNC_SYNCHRONIZING_ERROR 5
#define SIMBALS_SYNC_STOPPED 6
#define SIMBALS_SYNC_GENERAL_ERROR 7
#define SIMBALS_CONNECT_READY 8
#define SIMBALS_CONNECT_IDENTIFIED 9
#define SIMBALS_CONNECT_NOT_IDENTIFIED 10
#define SIMBALS_CONNECT_IDENTIFYING 11
#define SIMBALS_CONNECT_IDENTIFICATION_ERROR 12
#define SIMBALS_CONNECT_STOPPED 13
#define SIMBALS_CONNECT_GENERAL_ERROR 14
#define SIMBALS_MUSICID_READY 15
#define SIMBALS_MUSICID_IDENTIFIED 16
#define SIMBALS_MUSICID_NOT_IDENTIFIED 17
#define SIMBALS_MUSICID_IDENTIFYING 18
#define SIMBALS_MUSICID_IDENTIFICATION_ERROR 19
#define SIMBALS_MUSICID_STOPPED 20
#define SIMBALS_MUSICID_GENERAL_ERROR 21
#define SIMBALS_SWAPI_READY 22
#define SIMBALS_SWAPI_OPENING_SESSION 23
#define SIMBALS_SWAPI_NO_SESSION 24
#define SIMBALS_SWAPI_SESSION_OPENED 25
#define SIMBALS_SWAPI_JOB_NOT_STARTED 26
#define SIMBALS_SWAPI_JOB_STARTED 27
#define SIMBALS_SWAPI_JOB_FINISHED 28
#define SIMBALS_SWAPI_JOB_FAILED 29
#define SIMBALS_SWAPI_JOB_NOT_READY 30
#define SIMBALS_SWAPI_MUSICID_CALL_FAILED 31
#define SIMBALS_SWAPI_METADATA_NOT_FOUND 32
#define SIMBALS_SWAPI_METADATA_FOUND 33
#define SIMBALS_GENERAL_RECORDING 34
#define SIMBALS_GENERAL_RECORDING_ERROR 35
#define SIMBALS_GENERAL_RECORDING_SUCCESS 36
#define SIMBALS_GENERAL_AUDIOSESSION_ERROR 37
#define SIMBALS_GENERAL_INPUT_LEVEL_TOO_LOW 38 // > 35
#define SIMBALS_GENERAL_INPUT_LEVEL_LOW 39 // 30 35
#define SIMBALS_GENERAL_INPUT_LEVEL_MEDIUM 40 // 20 30
#define SIMBALS_GENERAL_INPUT_LEVEL_GOOD 41 // < 20
#define SIMBALS_LIVEMATCH_READY 42
#define SIMBALS_LIVEMATCH_IDENTIFIED 43
#define SIMBALS_LIVEMATCH_NOT_IDENTIFIED 44
#define SIMBALS_LIVEMATCH_IDENTIFYING 45
#define SIMBALS_LIVEMATCH_IDENTIFICATION_ERROR 46
#define SIMBALS_LIVEMATCH_STOPPED 47
#define SIMBALS_LIVEMATCH_GENERAL_ERROR 48
#define SIMBALS_SWAPI_LIVEMATCH_CALL_FAILED 49

@interface SimbalsMessages : NSObject
+ (void) initialize;
+ (NSString*) getTranslationFR:(int)message;
+ (NSString*) getTranslationEN:(int)message;

@end
