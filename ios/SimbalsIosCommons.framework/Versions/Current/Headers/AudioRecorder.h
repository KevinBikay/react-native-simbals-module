//
//  AudioRecorder.h
//  SimbalsIdentify
//
//  Created by florian on 03/12/13.
//  Copyright (c) 2013 Simbals. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@class AudioRecorder;

@protocol AudioRecorderDelegate;

@interface AudioRecorder : NSObject<AVAudioRecorderDelegate> {
@private
    id <AudioRecorderDelegate> _delegate;
    
    AVAudioRecorder* _recorder;
    NSString* _recorderFilePath;
    NSURL* _recordedTmpFile;
    NSTimer* _recorderInputLevelTimer;
    double _lowPassResult;
    int _countPass;
    bool _interrupted;
}

@property (retain,nonatomic) id delegate;

- (id) init:(NSString*) recorderPath;

- (void) start:(float)duration;
- (void) stop;
- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)success;
- (void) audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error;
- (bool) interrupted;

@end

/*!
 @protocol
 AudioRecorderDelegate
 @discussion
 This protocol is implemented by the public interface SimbalsIdentify
 */
@protocol AudioRecorderDelegate <NSObject>

@required
/*!
 This method is required and must be implemented in the delegate
 It is called each time a recorder finished
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the recording was successful
 */
- (void) audioRecorderDidFinish:(int)message successfully:(BOOL)success recordPath:(NSString*)recordPath;

@optional
/*!
 This method is optional and provides debug informations during the recording
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 */
- (void) audioRecorderMessages:(int)message;

@end