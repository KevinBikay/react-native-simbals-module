
#if __has_include("RCTBridgeModule.h")
    #import "RCTBridgeModule.h"
#else
    #import <React/RCTBridgeModule.h>
#endif

#import <AVFoundation/AVFoundation.h>
#import <SimbalsLiveMatch/SimbalsLiveMatch.h>
#import <SimbalsIosCommons/SimbalsMessages.h>
//#import <SimbalsIosCommons/AudioRecorder.h>

#import <React/RCTEventEmitter.h>

@interface RNSimbalsModule : NSObject <RCTBridgeModule> {
@private
	SimbalsLiveMatch* _liveMatch;
	NSArray* _meterColors;
	NSDictionary* _channelsList;
	BOOL _started;
	BOOL _shouldStop;
}

@end
