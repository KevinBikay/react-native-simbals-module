//
//  SimbalWebAPIClient.h
//  SimbalWebAPIClient
//
//  Created by florian on 12/03/2014.
//  Copyright (c) 2014 Simbals. All rights reserved.
//

/*!
 @header SimbalsWebAPIClient
 SymbalsWebAPIClient enables the use of Simbals webservices into ios applications
 @author Simbals 2014
 @copyright Simbals 2014
 @version 1.0
 @updated 2014-03-12
 */

#import <Foundation/Foundation.h>

@class SimbalsWebAPIClient;

@protocol SimbalsWebAPIClientDelegate;

@interface SimbalsWebAPIClient : NSObject {
    
@private
    id <SimbalsWebAPIClientDelegate> _delegate;
}

@property (retain,nonatomic) id delegate;

/*!
 constructor of the SimbalsWebAPIClient class.
 Use it when you want to constantly try to synchronize to the stream. Perform the initialization of the object and the initial setup.
 @param token
 The token you must use to access Simbals webservices
 @param baseUrl
 The base url of simbals web API (e.g., http://api.simbals.com)
 */
- (id) initWithToken:(NSString*)token baseUrl:(NSString*) baseUrl;

/*!
 Use this method to start musicid call
 */
- (void) startMusicId:(NSString*)pathToFingerprint;


/*!
 Use this method to start musicid call
 */
- (void) startLiveMatch:(NSString*)pathToFingerprint;


/*!
 Use this method to start metadata call
 */
- (void) getTrackMetadata:(NSString*)trackId;




/*!
 Use this method to start metadata call
 */
- (void) getLastPlayed:(NSString*)channelId;


@end

/*!
 @protocol
 SimbalsWebAPIClientDelegate
 @discussion
 This protocol has to be implemented by the front app to interact with the webservices
 */
@protocol SimbalsWebAPIClientDelegate <NSObject>

@optional

/*!
 This method is optional. It is called when the simbals musicid webservice has processed your query
 @param trackId
 provides the trackId corresponding to the track identified from the provided audio sample. Nil if uncessful
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the identification is successful
 */
- (void) simbalsWebAPIClientMusicIdDidFinish:(NSString*)trackId message:(int)message successfully:(BOOL)success;


/*!
 This method is optional. It is called when the simbals livematch webservice has processed your qrequest
 @param tchannelId
 provides the channelId corresponding to the channel identified from the provided audio sample. Nil if uncessful
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the identification is successful
 */
- (void) simbalsWebAPIClientLiveMatchDidFinish:(NSString*)channelId message:(int)message successfully:(BOOL)success;


/*!
 This method is optional. It is called when the Simbals metadata webservice has processed your query
 @param trackTitle
 provides the title of the track you asked for. Nil if uncessful
 @param artist
 provides the artist name of the track you asked for. Nil if uncessful
 @param album
 provides the album name of the track you asked for. Nil if uncessful
 @param coverUrl
 provides the url of the album cover of the track you asked for. Nil if uncessful
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the metadata query was successful
 */
- (void) simbalsWebAPIClientMetadataDidFinish:(NSString*)trackTitle artist:(NSString*)artist album:(NSString*)album coverUrl:(NSString*)coverUrl message:(int)message successfully:(BOOL)success;

/*!
 This method is optional. It is called when the Simbals metadata webservice has processed your query
 @param trackTitle
 provides the title of the track you asked for. Nil if uncessful
 @param artist
 provides the artist name of the track you asked for. Nil if uncessful
 @param album
 provides the album name of the track you asked for. Nil if uncessful
 @param coverUrl
 provides the url of the album cover of the track you asked for. Nil if uncessful
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 @param success
 whether the metadata query was successful
 */
- (void) simbalsWebAPIClientGetLastPLayedDidFinish:(NSString*)trackTitle artist:(NSString*)artist album:(NSString*)album coverUrl:(NSString*)coverUrl message:(int)message successfully:(BOOL)success;

/*!
 This method is optional and provides debug informations during the synchronization rounds
 @param message
 the message associated to this event. Message code reference is found on top of the doc
 */
- (void) simbalsWebAPIClientMessages:(int)message;

@end

